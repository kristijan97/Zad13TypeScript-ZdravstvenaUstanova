abstract class Person
{
    constructor(private firstName: string, private lastName: string)
    {
    }
}

interface IExamination
{
    Schedule(doc: Doctor, pat: Patient): Examination;
    GetResults(): any[];
    SetResults(values: any[]): void;
}

abstract class Examination
{
    public name: string;
    private date: string;
    private doctor: Doctor;
    private patient: Patient;

    constructor(n: string) {
        this.name = n;
    }

    Schedule(doc: Doctor, pat: Patient)
    {
        this.doctor = doc;
        this.patient = pat;
        this.date = new Date().toJSON().slice(0,10);

        return this;
    }

    abstract GetResults(): any[];
    abstract SetResults(values: any[]): void;
}

class BloodPressure extends Examination
{
    private low: number;
    private high: number;
    private pulse: number;

    constructor() {
        super("Blood pressure level");
    }

    SetResults(values: any[])
    {
        this.low = values[0];
        this.high = values[1];
        this.pulse = values[2];
    }

    GetResults()
    {
        return [this.low, this.high, this.pulse];
    }
}

class Doctor extends Person
{
    private specialty: string;
    private patientsList: number[] = [];

    constructor (public first: string, public last: string, spec: string)
    {
        super(first, last);
        this.specialty = spec;

        document.write(`Doctor ${this.first} ${this.last} is created<br />`);
    }

    AddPatient(cID: number): void
    {
        this.patientsList.push(cID);
    }

    RemovePatient(cID: number): void
    {
        let index = this.patientsList.indexOf(cID);
        if (index > -1)
            this.patientsList.slice(index, 1);
    }

    ScheduleExamination(e :Examination, pat: Patient)
    {
        var exam = e.Schedule(this, pat);
        document.write(`Doctor ${this.first} ${this.last} scheduled ${e.name} examination for patient ${pat.first} ${pat.last} <br />`);

        return exam;
    }
}

class Patient extends Person
{
    private jmbg: number;
    public cartoonID: number;
    private doctor: Doctor;

    constructor (public first: string, public last: string, jmbg: number, cID: number)
    {
        super(first, last);
        this.jmbg = jmbg;
        this.cartoonID = cID;

        document.write(`Patient ${this.first} ${this.last} is created<br />`);
    }

    ChooseDoctor(doc: Doctor): void
    {
        if (doc != this.doctor)
        {
            if(this.doctor != undefined)
                doc.RemovePatient(this.cartoonID);

            this.doctor = doc;
            doc.AddPatient(this.cartoonID);
            document.write(`Patient ${this.first} ${this.last} choose doctor ${this.doctor.first} ${this.doctor.last}<br />`);
        }
        else
            alert('You already choose this doctor!');
    }

    private Random(min, max): number
    {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    GoToDoctor(exam: Examination): void
    {
        if(exam.name == "Blood pressure level")
        {
            exam.SetResults([this.Random(60, 80), this.Random(100, 120), this.Random(60, 150)]);
            let results: any[] = exam.GetResults();

            document.write(`Patient ${this.first} ${this.last} has done ${exam.name} examination with results: Low - ${results[0]}, High - ${results[1]}, Pulse - ${results[2]}<br />`);
        }
    }
}

let doctor = new Doctor('Milan', 'Milankovic', 'pedijatar');
let patient = new Patient('Perica', 'Peric', 1337, 1);

patient.ChooseDoctor(doctor);

let exam1 = doctor.ScheduleExamination(new BloodPressure(), patient);

patient.GoToDoctor(exam1);