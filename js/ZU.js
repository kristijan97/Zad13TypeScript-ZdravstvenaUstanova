var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Person = (function () {
    function Person(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    return Person;
}());
var Examination = (function () {
    function Examination(n) {
        this.name = n;
    }
    Examination.prototype.Schedule = function (doc, pat) {
        this.doctor = doc;
        this.patient = pat;
        this.date = new Date().toJSON().slice(0, 10);
        return this;
    };
    return Examination;
}());
var BloodPressure = (function (_super) {
    __extends(BloodPressure, _super);
    function BloodPressure() {
        _super.call(this, "Blood pressure level");
    }
    BloodPressure.prototype.SetResults = function (values) {
        this.low = values[0];
        this.high = values[1];
        this.pulse = values[2];
    };
    BloodPressure.prototype.GetResults = function () {
        return [this.low, this.high, this.pulse];
    };
    return BloodPressure;
}(Examination));
var Doctor = (function (_super) {
    __extends(Doctor, _super);
    function Doctor(first, last, spec) {
        _super.call(this, first, last);
        this.first = first;
        this.last = last;
        this.patientsList = [];
        this.specialty = spec;
        document.write("Doctor " + this.first + " " + this.last + " is created<br />");
    }
    Doctor.prototype.AddPatient = function (cID) {
        this.patientsList.push(cID);
    };
    Doctor.prototype.RemovePatient = function (cID) {
        var index = this.patientsList.indexOf(cID);
        if (index > -1)
            this.patientsList.slice(index, 1);
    };
    Doctor.prototype.ScheduleExamination = function (e, pat) {
        var exam = e.Schedule(this, pat);
        document.write("Doctor " + this.first + " " + this.last + " scheduled " + e.name + " examination for patient " + pat.first + " " + pat.last + " <br />");
        return exam;
    };
    return Doctor;
}(Person));
var Patient = (function (_super) {
    __extends(Patient, _super);
    function Patient(first, last, jmbg, cID) {
        _super.call(this, first, last);
        this.first = first;
        this.last = last;
        this.jmbg = jmbg;
        this.cartoonID = cID;
        document.write("Patient " + this.first + " " + this.last + " is created<br />");
    }
    Patient.prototype.ChooseDoctor = function (doc) {
        if (doc != this.doctor) {
            if (this.doctor != undefined)
                doc.RemovePatient(this.cartoonID);
            this.doctor = doc;
            doc.AddPatient(this.cartoonID);
            document.write("Patient " + this.first + " " + this.last + " choose doctor " + this.doctor.first + " " + this.doctor.last + "<br />");
        }
        else
            alert('You already choose this doctor!');
    };
    Patient.prototype.Random = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    Patient.prototype.GoToDoctor = function (exam) {
        if (exam.name == "Blood pressure level") {
            exam.SetResults([this.Random(60, 80), this.Random(100, 120), this.Random(60, 150)]);
            var results = exam.GetResults();
            document.write("Patient " + this.first + " " + this.last + " has done " + exam.name + " examination with results: Low - " + results[0] + ", High - " + results[1] + ", Pulse - " + results[2] + "<br />");
        }
    };
    return Patient;
}(Person));
var doctor = new Doctor('Milan', 'Milankovic', 'pedijatar');
var patient = new Patient('Perica', 'Peric', 1337, 1);
patient.ChooseDoctor(doctor);
var exam1 = doctor.ScheduleExamination(new BloodPressure(), patient);
patient.GoToDoctor(exam1);
